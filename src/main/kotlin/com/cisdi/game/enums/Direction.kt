package com.cisdi.game.enums

enum class Direction {
    UP,DOWN,LEFT,RIGHT
}