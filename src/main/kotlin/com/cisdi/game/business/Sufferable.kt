package com.cisdi.game.business

import com.cisdi.game.model.View

interface Sufferable : View {

    val blood: Int

    fun notifySuffer(attackable: Attackable):Array<View>?
}