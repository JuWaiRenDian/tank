package com.cisdi.game.business

import com.cisdi.game.Config
import com.cisdi.game.enums.Direction
import com.cisdi.game.model.View

/**
 * 运动能力
 */
interface Movable: View {

    //当前运动方向
    var currentDirection: Direction

    //碰撞方向
    var badDirection:Direction?

    //移动速度
    val speed: Int

    //获取将会碰撞的方向
    fun wilClollision(block: Blockable): Direction?

    //通知发生碰撞
    fun notifyCollision(direction: Direction?, block: Blockable)

    //进行移动
    fun move(direction: Direction) {

        //当前的方向和移动方向不一致时，只做方向改变
        if (currentDirection != direction) {
            currentDirection = direction
            return
        }

        //碰撞判断
        if (direction == badDirection){
            //不走了
            return
        }

        //进行移动
        when (currentDirection) {
            Direction.UP -> y -= speed
            Direction.DOWN -> y += speed
            Direction.LEFT -> x -= speed
            Direction.RIGHT -> x += speed
        }

        //越界判断
        if (x < 0) x = 0
        if (x > Config.gameWidth - width) x = Config.gameWidth - width
        if (y < 0) y = 0
        if (y > Config.gameHeight - height) y = Config.gameHeight - height

    }
}