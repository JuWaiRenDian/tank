package com.cisdi.game.business

import com.cisdi.game.enums.Direction
import com.cisdi.game.model.View

/**
 * 自动移动的能力
 */
interface AutoMovable : View {

    val currentDirection: Direction

    val speed: Int

    fun automove()
}