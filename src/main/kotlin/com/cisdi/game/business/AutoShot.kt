package com.cisdi.game.business

import com.cisdi.game.model.View

/**
 * 自动射击
 */
interface AutoShot :View{

    fun autoShot():View?
}