package com.cisdi.game.business

import com.cisdi.game.model.View

interface Attackable :View {

    var power:Int

    var owner:View

    fun isCollision(sufferable: Sufferable):Boolean

    fun notifyAttack(sufferable: Sufferable)
}