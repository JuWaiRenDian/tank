package com.cisdi.game

import com.cisdi.game.business.*
import com.cisdi.game.enums.Direction
import com.cisdi.game.model.*
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import org.itheima.kotlin.game.core.Window
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.util.concurrent.CopyOnWriteArrayList


class GameWindow: Window(title = "坦克大战1.0",width = Config.gameWidth,height = Config.gameHeight) {

    private var views = CopyOnWriteArrayList<View>()

    //玩家坦克
    private lateinit var tank:Tank

    //游戏结束标识
    private var gameover:Boolean = false

    //敌方坦克总数
    private var enemysTotalize:Int = 20

    //界面最多显示敌方坦克数量
    private var maxEnemySize:Int = 3

    //敌方的出生点
    private var enmeyBornLocation = arrayListOf<Pair<Int,Int>>()

    private var bornIndex = 0


    override fun onCreate() {
        //地图 通过读文件的方式创建地图
//        var file = File(javaClass.getResource("/map/1.map").path)
        val resourceAsStream = javaClass.getResourceAsStream("/map/1.map")
        val reader = BufferedReader(InputStreamReader(resourceAsStream,"utf-8"))
        //读取文件行
        val lines:List<String> = reader.readLines()
        var lineNum = 0
        lines.forEach{
            var columNum = 0
            it.toCharArray().forEach {
                when(it){
                    '砖' -> views.add(Wall(columNum*Config.block,lineNum*Config.block))
                    '草' -> views.add(Grass(columNum*Config.block,lineNum*Config.block))
                    '水' -> views.add(Water(columNum*Config.block,lineNum*Config.block))
                    '铁' -> views.add(Steel(columNum*Config.block,lineNum*Config.block))
                    '敌' -> enmeyBornLocation.add(Pair(columNum*Config.block,lineNum*Config.block))
                }
                columNum ++
            }
            lineNum ++
        }
        tank = Tank(Config.block*10,Config.block*12)
        //添加大本营
        var camp = Camp((Config.gameWidth-120)/2,Config.gameHeight-90)
        views.add(tank)
        views.add(camp)

    }

    override fun onDisplay() {
        views.forEach{
            it.draw()
        }
    }

    override fun onKeyPressed(event: KeyEvent) {
        if (!gameover){
            //用户操作
            when (event.code) {
                KeyCode.UP -> tank.move(Direction.UP)
                KeyCode.DOWN -> tank.move(Direction.DOWN)
                KeyCode.LEFT -> tank.move(Direction.LEFT)
                KeyCode.RIGHT -> tank.move(Direction.RIGHT)
                KeyCode.ENTER -> {
                    val bullet = tank.shot()
                    views.add(bullet)
                }


            }
        }

    }

    override fun onRefresh() {


        //检测销毁
        views.filter { it is Distoryable }.forEach{
            if((it as Distoryable).isDestory()){
                views.remove(it)
                if (it is Enemy){
                    enemysTotalize --
                }
                val destory = it.showDestory()
                destory?.let {
                    views.add(destory)
                }
            }
        }

        //显示game over
        views.filter { it is GameOver }.forEach {
            it.draw()
        }

        //游戏结束判断
        if (gameover){
            return
        }

        //找到可移动的物体
        views.filter { it is Movable }.forEach{ move ->

            move as Movable
            move.badDirection = null
            var badDirection:Direction? = null
            //找到阻塞物体
            views.filter { (it is Blockable) and (move != it)}.forEach blockTag@{ block ->

                block as Blockable

                //判断是否碰撞 返回碰撞方向
                val direction: Direction? = move.wilClollision(block)

                direction?.let {
                    badDirection = direction
                    move.notifyCollision(badDirection,block)
                    return@blockTag
                }

            }

            //找到阻塞块和移动块和碰撞方向
        }

        //检测自动移动的物体
        views.filter { it is AutoMovable }.forEach{
            (it as AutoMovable).automove()
        }

       //检测具备攻击能力和被攻击能力的物体是否发生碰撞
        views.filter { it is Attackable }.forEach{attack ->
            attack as Attackable
            views.filter { (it is Sufferable) and (attack.owner != it) and (attack != it) }.forEach sufferTag@ {suffer ->
                suffer as Sufferable
                if (attack.isCollision(suffer)){
                    //通知攻击者产生碰撞
                    attack.notifyAttack(suffer)
                    //通知被攻击者产生碰撞
                    var suffers = suffer.notifySuffer(attack)
                    suffers?.let { views.addAll(it) }

                }
            }
        }

        //检测自动发射的子弹
        views.filter { it is AutoShot }.forEach {
            it as AutoShot
            val shot:View? = it.autoShot()
            shot?.let {
                views.add(shot)
            }
        }

        //检测游戏是否结束
        if ((views.filter { it is Camp }.isEmpty()) or  (enemysTotalize<=0)){
            gameover = true
            views.add(GameOver())
        }

        //检测地方出生
        //判断页面敌方数量
        if(views.filter { it is Enemy }.size < maxEnemySize){
            var index = bornIndex % enmeyBornLocation.size
            val pair = enmeyBornLocation[index]
            views.add(Enemy(pair.first,pair.second))
            bornIndex ++
        }

    }
}