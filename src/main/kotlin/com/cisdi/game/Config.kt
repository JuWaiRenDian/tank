package com.cisdi.game

import com.cisdi.game.model.Bullet

//Object 单例
object Config {
    val gameWidth:Int = 60*13
    val gameHeight:Int = 60*13

    val block:Int = 60

    val bullet:Int = 16

    val blastH:Int = 107
    val blastW:Int = 136
}