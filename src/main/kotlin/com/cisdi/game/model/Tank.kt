package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.Attackable
import com.cisdi.game.business.Blockable
import com.cisdi.game.business.Movable
import com.cisdi.game.business.Sufferable
import com.cisdi.game.enums.Direction
import org.itheima.kotlin.game.core.Composer
import org.itheima.kotlin.game.core.Painter

class Tank(override var x: Int, override var y: Int) : Movable,Blockable,Sufferable{
    override var blood: Int = 50

    override var badDirection: Direction? = null

    override var height: Int = Config.block

    override var width: Int = Config.block

    //方向 默认UP
    override var currentDirection: Direction = Direction.UP

    override val speed: Int = 10

    override fun draw() {

        var imagePath = when (currentDirection) {
            Direction.UP -> "img/p1tankU.gif"
            Direction.DOWN -> "img/p1tankD.gif"
            Direction.LEFT -> "img/p1tankL.gif"
            Direction.RIGHT -> "img/p1tankR.gif"
        }

        Painter.drawImage(imagePath, x, y)
    }

    override fun wilClollision(block: Blockable): Direction? {

        //将要碰撞
        var willX = this.x
        var willy = this.y

        when (currentDirection) {
            Direction.UP -> willy -= speed
            Direction.DOWN -> willy += speed
            Direction.LEFT -> willX -= speed
            Direction.RIGHT -> willX += speed
        }

        var collision  = checkCollision(block.x,block.y,block.width,block.height
                                        ,willX,willy,width,height)


        return if (collision) currentDirection else null
    }

    //通知有碰撞方向
    override fun notifyCollision(direction: Direction?, block: Blockable) {
        //TODO 接收碰撞信息
        this.badDirection = direction
    }

    //发射子弹
    fun shot():View{
        Composer.play("img/fire.wav")
        var bulletX:Int = 0
        var bulletY:Int = 0
        //判断子弹的位置
        when(currentDirection){
            Direction.UP -> {
                bulletX = x + (width - Config.bullet)/2
                bulletY = y - Config.bullet/2
            }
            Direction.DOWN -> {
                bulletX = x + (width - Config.bullet)/2
                bulletY = y + height - Config.bullet/2
            }
            Direction.LEFT -> {
                bulletX = x - Config.bullet/2
                bulletY = y + (height-Config.bullet)/2
            }
            Direction.RIGHT -> {
                bulletX = x + width - Config.bullet/2
                bulletY = y + (height-Config.bullet)/2
            }
        }
        return Bullet(this,bulletX,bulletY,currentDirection)


    }

    override fun notifySuffer(attackable: Attackable): Array<View>? {
        println("墙遭到碰撞")
        Composer.play("img/hit.wav")
        blood -= attackable.power
        return arrayOf(Blast(x, y))
    }

}