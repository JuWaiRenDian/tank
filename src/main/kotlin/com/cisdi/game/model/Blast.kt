package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.Distoryable
import org.itheima.kotlin.game.core.Painter

/**
 * 爆炸
 */
class Blast(override var x: Int, override var y: Int, var blastType: Int = 0) : Distoryable {

    override var height: Int = Config.block

    override var width: Int = Config.block


    var imageList: ArrayList<String> = arrayListOf()

    private var index: Int = 0

    init {
        if (blastType == 0) {
            for (i in 1..4) {
                imageList.add("img/born${i}.gif")
            }
        } else {
            for (i in 1..8) {
                imageList.add("img/blast${i}.gif")
            }
        }
    }


    override fun draw() {
        var i = index % imageList.size
        Painter.drawImage(imageList[i], x, y)
        index++

    }

    override fun isDestory(): Boolean = index >= imageList.size


}