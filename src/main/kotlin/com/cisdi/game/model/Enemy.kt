package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.*
import com.cisdi.game.enums.Direction
import org.itheima.kotlin.game.core.Composer
import org.itheima.kotlin.game.core.Painter
import java.util.*


class Enemy(override var x: Int, override var y: Int) :Movable,AutoMovable,Blockable, AutoShot, Sufferable,Distoryable {

    override var blood: Int =5

    override var currentDirection: Direction = Direction.DOWN

    override var badDirection: Direction? = null

    override val speed: Int = 2

    override var height: Int = Config.block

    override var width: Int = Config.block

    private var lastShotTime = 0L

    private var shotFrequence = 2000



    var index:Int = 0

    init {
        index = (1..3).random()
    }

    override fun draw() {

        var imagePath = when (currentDirection) {
            Direction.UP -> "img/enemy${index}U.gif"
            Direction.DOWN -> "img/enemy${index}D.gif"
            Direction.LEFT -> "img/enemy${index}L.gif"
            Direction.RIGHT -> "img/enemy${index}R.gif"
        }

        Painter.drawImage(imagePath, x, y)
    }

    override fun wilClollision(block: Blockable): Direction? {
        //将要碰撞
        var willX = this.x
        var willy = this.y

        when (currentDirection) {
            Direction.UP -> willy -= speed
            Direction.DOWN -> willy += speed
            Direction.LEFT -> willX -= speed
            Direction.RIGHT -> willX += speed
        }

        var collision  = checkCollision(block.x,block.y,block.width,block.height
            ,willX,willy,width,height)


        return if (collision) currentDirection else null
    }

    override fun notifyCollision(direction: Direction?, block: Blockable) {
        //TODO 接收碰撞信息
        this.badDirection = direction
    }

    override fun automove() {

        //判断当前方向和碰撞方向是否一致
        if (currentDirection == badDirection){
            currentDirection = randomDirection(badDirection)
            return
        }

        //进行移动
        when (currentDirection) {
            Direction.UP -> y -= speed
            Direction.DOWN -> y += speed
            Direction.LEFT -> x -= speed
            Direction.RIGHT -> x += speed
        }

        //越界判断
        if (x < 0) currentDirection = Direction.RIGHT
        if (x > Config.gameWidth - width) currentDirection = Direction.LEFT
        if (y < 0) currentDirection = Direction.DOWN
        if (y > Config.gameHeight - height) currentDirection = Direction.UP
    }

    private fun randomDirection(bad: Direction?):Direction{
        var i  = Random().nextInt(4)
        var direction = when(i){
            0 -> Direction.UP
            1 -> Direction.DOWN
            2 -> Direction.LEFT
            3 -> Direction.RIGHT
            else -> Direction.UP
        }
        if (direction == bad){
            return randomDirection(bad)
        }
        return direction;
    }

    /**
     * 自动射击
     */
    override fun autoShot(): View? {

        var current = System.currentTimeMillis()

        if (current -lastShotTime < shotFrequence) return null

        lastShotTime = current

        var bulletX:Int = 0
        var bulletY:Int = 0
        //判断子弹的位置
        when(currentDirection){
            Direction.UP -> {
                bulletX = x + (width - Config.bullet)/2
                bulletY = y - Config.bullet/2
            }
            Direction.DOWN -> {
                bulletX = x + (width - Config.bullet)/2
                bulletY = y + height - Config.bullet/2
            }
            Direction.LEFT -> {
                bulletX = x - Config.bullet/2
                bulletY = y + (height-Config.bullet)/2
            }
            Direction.RIGHT -> {
                bulletX = x + width - Config.bullet/2
                bulletY = y + (height-Config.bullet)/2
            }
        }
        return Bullet(this,bulletX, bulletY, currentDirection)
    }

    override fun notifySuffer(attackable: Attackable): Array<View>? {
        println("墙遭到碰撞")
        Composer.play("img/hit.wav")
        blood -= attackable.power
        return arrayOf(Blast(x, y))
    }

    override fun isDestory(): Boolean = blood <= 0

    override fun showDestory(): View? {
        return Blast(x,y,1)
    }


}