package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.Attackable
import com.cisdi.game.business.Blockable
import com.cisdi.game.business.Distoryable
import com.cisdi.game.business.Sufferable
import org.itheima.kotlin.game.core.Painter

/**
 * 大本营
 */
class Camp(override var x: Int, override var y: Int) :Sufferable,Distoryable,Blockable {

    override var blood: Int = 12

    override var height: Int = 90

    override var width: Int = 120


    override fun draw() {

        if (blood <= 3){
            Painter.drawImage("img/symbol.gif",x+30,y+45)
        }else if (blood <= 6){
            //第一列
            Painter.drawImage("img/wall/wall.gif",x,y)
            Painter.drawImage("img/wall/wall.gif",x,y+30)
            Painter.drawImage("img/wall/wall.gif",x,y+60)

            //第一行
            Painter.drawImage("img/wall/wall.gif",x+30,y)
            Painter.drawImage("img/wall/wall.gif",x+60,y)
            Painter.drawImage("img/wall/wall.gif",x+90,y)

            //第二列
            Painter.drawImage("img/wall/wall.gif",x+90,y+30)
            Painter.drawImage("img/wall/wall.gif",x+90,y+60)
            Painter.drawImage("img/symbol.gif",x+30,y+45)
        }else{
            //第一列
            Painter.drawImage("img/steel.gif",x,y)
            Painter.drawImage("img/steel.gif",x,y+30)
            Painter.drawImage("img/steel.gif",x,y+60)

            //第一行
            Painter.drawImage("img/steel.gif",x+30,y)
            Painter.drawImage("img/steel.gif",x+60,y)
            Painter.drawImage("img/steel.gif",x+90,y)

            //第二列
            Painter.drawImage("img/steel.gif",x+90,y+30)
            Painter.drawImage("img/steel.gif",x+90,y+60)
            Painter.drawImage("img/symbol.gif",x+30,y+45)
        }



    }

    override fun isDestory(): Boolean = blood <= 0
    //遭受打击
    override fun notifySuffer(attackable: Attackable): Array<View>? {
        blood -= attackable.power
        return null
    }

    override fun showDestory(): View? {
        return Blast(x,y,1)
    }

}