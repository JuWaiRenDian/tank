package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.Attackable
import com.cisdi.game.business.Blockable
import com.cisdi.game.business.Sufferable
import org.itheima.kotlin.game.core.Painter

class Steel(override var x: Int, override var y: Int) :Blockable,Sufferable{
    override val blood: Int = 1

    override var height: Int = Config.block

    override var width: Int = Config.block


    override fun draw() {
        Painter.drawImage("img/steels.gif",x,y)
    }

    //铁墙遭受打击时，不变化
    override fun notifySuffer(attackable: Attackable): Array<View>? {
        return null
    }

}