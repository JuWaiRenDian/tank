package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.Blockable
import org.itheima.kotlin.game.core.Painter

class Water(override var x: Int, override var y: Int):Blockable {

    override var height: Int = Config.block

    override var width: Int = Config.block


    override fun draw() {
        Painter.drawImage("img/water.gif",x,y)
    }

}