package com.cisdi.game.model

import com.cisdi.game.Config
import org.itheima.kotlin.game.core.Painter

/**
 * 游戏结束图标
 */
class GameOver() :View {

    override var height: Int = 45
    override var width: Int = 80
    override var x: Int = (Config.gameWidth-width)/2
    override var y: Int = (Config.gameWidth-height)/2

    override fun draw() {
        Painter.drawImage("img/over.gif", x, y)
    }
}