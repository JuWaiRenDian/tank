package com.cisdi.game.model

import com.cisdi.game.enums.Direction

/**
 * 规范定义
 */
interface View {

    //position
    var x:Int
    var y:Int
    //height width
    var height:Int
    var width:Int
    //show

    fun draw()

    fun checkCollision(x1:Int,y1:Int,w1:Int,h1:Int
                    ,x2:Int,y2:Int,w2:Int,h2:Int):Boolean{
        return when {
            y2 + h2 <= y1 -> false
            y1 + h1 <= y2 -> false
            x2 + w2 <= x1 -> false
            x1 + w1 <= x2 -> false
            else -> true
        }
    }
}