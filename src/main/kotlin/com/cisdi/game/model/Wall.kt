package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.Attackable
import com.cisdi.game.business.Blockable
import com.cisdi.game.business.Distoryable
import com.cisdi.game.business.Sufferable
import org.itheima.kotlin.game.core.Composer
import org.itheima.kotlin.game.core.Painter

class Wall(override var x: Int, override var y: Int) : Blockable, Sufferable, Distoryable {

    override var height: Int = Config.block

    override var width: Int = Config.block

    override var blood: Int = 3

    override fun draw() {
        Painter.drawImage("img/walls.gif", x, y)
    }

    override fun isDestory(): Boolean = blood <= 0


    override fun notifySuffer(attackable: Attackable):Array<View> {
        println("墙遭到碰撞")
        Composer.play("img/hit.wav")
        blood -= attackable.power
        return arrayOf(Blast(x, y))
    }


}