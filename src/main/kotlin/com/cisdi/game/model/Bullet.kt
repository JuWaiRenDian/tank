package com.cisdi.game.model

import com.cisdi.game.Config
import com.cisdi.game.business.Attackable
import com.cisdi.game.business.AutoMovable
import com.cisdi.game.business.Distoryable
import com.cisdi.game.business.Sufferable
import com.cisdi.game.enums.Direction
import com.cisdi.game.ext.checkCollision
import org.itheima.kotlin.game.core.Composer
import org.itheima.kotlin.game.core.Painter

/**
 * 子弹
 */
class Bullet(override var owner: View,override var x: Int, override var y: Int, override var currentDirection: Direction
) : AutoMovable, Distoryable,Attackable,Sufferable {

    override val speed: Int = 8

    override var blood: Int = 1

    override var height: Int = Config.bullet
    override var width: Int = Config.bullet

    private var isDestory:Boolean = false

   //攻击力
    override var power: Int = 1


    override fun draw() {
        Painter.drawImage("img/tankmissile.gif", x, y)
    }

    override fun automove() {
        when (currentDirection) {
            Direction.UP -> y -= speed
            Direction.DOWN -> y += speed
            Direction.LEFT -> x -= speed
            Direction.RIGHT -> x += speed
        }
    }

    override fun isDestory(): Boolean {
        if (isDestory) return true
        if (x > Config.gameWidth) return true
        if (x < -width) return true
        if (y > Config.gameHeight) return true
        if (y < -height) return true
        else return false
    }


    override fun isCollision(sufferable: Sufferable): Boolean {
        return checkCollision(sufferable)
    }

    override fun notifyAttack(sufferable: Sufferable) {
        println("子弹受到碰撞")
        isDestory = true

    }

    override fun notifySuffer(attackable: Attackable): Array<View>? {
        blood -= attackable.power
        if (blood<=0){
            isDestory = true
        }
        return arrayOf(Blast(x, y))
    }

}